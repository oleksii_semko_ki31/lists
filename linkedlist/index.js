// --- Directions
// Implement classes Node and Linked Lists
// See 'directions' document

class Node {
    constructor(data, next=null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
    }

    insertFirst(data) {
        this.head = new Node(data, this.head)
    }

    size() {
        let node = this.head;
        let count = 0;

        while (node) {
            node = node.next;
            count++;
        }

        return count;
    }

    getFirst() {
        return this.head;
    }

    getLast() {
        if(!this.head) {
            return null;    
        }

        let node = this.head;
        while(node.next) {
            node = node.next;
        }
        return node;
    }

    clear() {
        this.head = null;
    }

    removeFirst() {
        if(this.head) {
            this.head = this.head.next;
        }
    }

    removeLast() {
        if (!this.head) {
            return;
        }

        if (!this.head.next) {
            this.head = null;
        }

        else {
            let node = this.head.next;
            let previous = this.head;

            while(node.next) {
                previous = node;
                node = node.next;
            }
            previous.next = null;
        }
    }

    insertLast(data) {
        const lastNode = this.getLast();
        if(lastNode) {
            lastNode.next = new Node(data);
        }

        else {
            this.head = new Node(data);
        }
    }

    getAt(index) {
        let node = this.head;
        let count = 0;
        while(node) {
            if (count === index) {
                return node;
            }
            node = node.next;
            count++;
        }
        return null;
    }

    removeAt(index) {
        if (!this.head) {
            return;
        }

        else if(index===0) {
            this.head = this.head.next;
            return;
        }

        const node = this.getAt(index-1);
        if(node && node.next) {
            node.next = node.next.next;
        }
    }

    insertAt(data, index) {
        if (!this.head) {
            this.head = new Node(data);
            return;
        }

        else if(index === 0) {
            this.head = new Node(data, this.head);
            return;
        }

        let node = this.getAt(index-1) || this.getLast();
        node.next = new Node(data, node.next);

    }

    forEach(func) {
        let node = this.head;
        while (node) {
            func(node);
            node = node.next;
        }
    }

    *[Symbol.iterator]() {
        let node = this.head;

        while(node) {
            yield node;
            node = node.next;
        }
    }
}

module.exports = { Node, LinkedList };
